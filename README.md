# Formations Estrades

## De quoi s'agit-il ?

Ce dépôt recence des supports de formation proposés par la plateforme Estrades sur différentes technologies ou outils qui entrent dans le cadre de l'édition numérique de corpus. 

Il s'agit de **supports** et non de **tutoriels** : les supports présentés ici sont censés accompagner une présentation plus détaillée à l'oral par un.e formateur.ice.

## Contributeur.ice.s

Les membres de la plateforme : [https://estrades.huma-num.fr/site/equipe.html](https://estrades.huma-num.fr/site/equipe.html)

## Réutilisation

Si cela vous intéresse, vous pouvez réutiliser ces supports dans le cadre de la licence **CC BY-SA**.

## Fichiers

- 1 fichier = 1 formation sur un thème 
- les supports sont écrits en marKdown
- en entête des fichiers, on pose un bloc de métadonnées sous cette forme : 

```
---
Contributeur.ice.s: Prénom Nom
Date de création: AAAA ou AAAA-MM ou AAAA-MM-JJ
Modificatons: AAAA ou AAAA-MM ou AAAA-MM-JJ
Titre: TITRE
Licence: CC BY-SA
Description: blablabla
---
```


