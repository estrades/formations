# Introduction à l'OCR/HTR​

Edition structuré ? [Exemple](https://num-arche.unistra.fr/tjem/accueil​)

Etape nécessaire : passer du document (numérisé) à un fichier texte. Une solution : l'HTR/OCR

## l'OCR/HTR

![chaine OCR](images/chaine_ocr_1_.png )

## Présentation des documents

- [Courier du Bas-Rhin - Niederrheinischer Kurier]( https://gallica.bnf.fr/ark:/12148/bd6t52694199/f1.item) ​
- XVIII et XIX siècles​
- Aujourd'hui : 7 juin 1814​
- Charte constitutionnelle du 4 juin 1814

## Réflexions 

- Quels problèmes à anticiper pour l'OCR ?
- Quelle ontologie pour la première page ?

## Exemple de résultat final

![results htr](images/results_htr_1_.png)

## On met les mains dedans !

https://escriptorium.unistra.fr/​

## Comparer différents modèles

Où en trouver ? HTR United, Zenodo, Huggingface,...​
Choix du modèle avec comparaison dans escripto​

## Et maintenant, la segmentation

- Segmentation de zones​
- Segmentation de lignes​
- En utilisant l'ontologie du début

## Transcription

- Avec le meilleur modèle
- Et corrections manuelles

## Export

Différents formats pour différents buts​
- Texte​
- PAGE XML​
- XML ALTO​


## Ce qu'on peut faire en plus

- Finetune pour des tâches spécifiques
- Partager les données sur différents dépôts

![data in htrunited](images/htrunited.png)



## Pour les plus téméraires

Re-essayez la pipeline entière avec vos propres documents ! 