---
Contributeur.ice.s: Guillaume Porte
Date de création: 2023
Modifications: 2024-06
Titre: XQuery
Licence: CC BY-SA
Description: support pour une formation en présentiel à XSLT - niveau débutant.
---

# XQuery

"XQuery est un langage de requête informatique permettant non seulement d'extraire des informations d'un document XML, ou d'une collection de documents XML, mais également d'effectuer des calculs complexes à partir des informations extraites et de reconstruire de nouveaux documents ou fragments XML." ([wikipedia](https://fr.wikipedia.org/wiki/Extensible_Stylesheet_Language_Transformations)) 

- XQuery a une syntaxe propre et ne s'écrit pas en XML (à la différence de XSL)
- les fichiers XQuery ont l'extension `.xq` ou `.xqm`

[BaseX](https://basex.org/) est un logiciel open-source qui permet, entre autres, d'éxécuter des requêtes XQuery. On appelle ce type de logiciel des "bases de données XML natives" car ils permettent d'indexer des données XML et de les traiter _un peu comme_ on le ferait avec des données relationnelles SQL.

Alternatives à BaseX: [eXist-db](http://exist-db.org/exist/apps/homepage/index.html), MarkLogic, etc. (voir [ici](https://fr.wikipedia.org/wiki/Base_de_donn%C3%A9es_XML_native))


## Charger un document dans BaseX

1. ouvrir Basex GUI

2. dans la  barre de menu suivante, sélectionner uniquement le crayon et le `<x>`

![graphic](images/barrebasexgui.png)

3. dans la barre de menu suivante, cliquer sur le logo page (ou `Editor > New` ou `ctrl+t` ou `cmd+t`)

![graphic](images/barrebasexgui2.png)

4. dans l’éditeur qui vient de s’ouvrir, insérer les lignes suivantes :

```
let $document := doc("DossierDeFormation/ressources/bookstore.xml")
return($document)
```

**remplacer "Dossier de formation" par bon chemin**_

- `let $document :=` créé une _variable_ ([quésaco](https://fr.wikipedia.org/wiki/Variable_(informatique))?) nommée `$document` dont le contenu est égal à (`:=`) à ce qui est chargé avec l'instruction suivante (`doc()`)
- `doc()` charge le document contenu dans les parenthèses (en lo'occurrence `bookstore.xml`)
- `return` renvoie le résultat

**dis autrement : ** la variable `$document` contient l'intégralité du document `bookstore.xml`

`doc()` permet aussi de charger un document en ligne :

```
let $document := doc("https://gitlab.huma-num.fr/estrades/formations/-/raw/main/ressources/bookstore.xml")
return($document)
```

5. parcourir un arbre XML avec `for `:

```
let $document := doc("DossierDeFormation/bookstore.xml")
for $livre in $document//*:book
return($livre)
```

`for $livre in $document//*:book` = à chaque fois que notre script trouve un élément `<book>` dans `$document`, il crée une variable `$livre` qui contient l’intégralité de l'élément `<book>` 

- 1ère itération de la boucle, `$livre` = 
```
<book category="XML">
  <title lang="fr">Oxygen, quel enfer</title>
  <author>Elsa Van Kote</author>
  <year>2023</year>
  <price>25</price>
</book>
```

- 2ème itération de la boucle, `$livre` = 
```
<book category="lifestyle">
  <title lang="fr">Open-Source et lolcats</title>
  <author>Régis Witz</author>
  <year>2018</year>
  <price>0</price>
</book>
```

_etc._

Cette variable peut-être interrogée en XPATH de cette manière `$book/author` ou encore `$book/year[.>2015]`

6. **on teste :** pour chaque `<book>`, afficher  (`return`) la valeur de `<title>`

7. trier les résultats :

```
let $document := doc("DossierDeFormation/bookstore.xml")
for $livre in $document//*:book
let $titre := $livre/*:title
order by $titre
return($livre)
```

`order by` : effectue un tri. On peut préciser `ascending` (par défaut) ou `descending` après la variable

8. **on teste :** trier les résultats par `<author>`

9. comparer les résultats en changeant le contenu du return

    - a : `$livre/*:title`
    - b : `$titre`
    - c : `data($titre)`

10. concaténer des résultats

`return('livre : ' || $titre )`

11. **on teste :** renvoyer les résultats sous cette forme `Livre : livre, année.`

12. filtrer les résultats

```
let $document := doc("DossierDeFormation/bookstore.xml")
for $livre in $document//*:book
where $livre/year > 2020
let $titre := $livre/*:title
order by $titre
return($titre || ', ' || $livre/*:year)
```

`where` : ajoute un filtre à la requête : on cherche tous les éléments qui répondent au critère défini dans `where`

13. **on teste :** chercher tous les livres dont le prix est inférieur à 30, triez-les par année et afficher les résultats sous la forme `auteur, titre, année (prix)`.

_Un problème avec l’affichage du nom d’auteur ?_

14. XQuery peut utiliser de nombreuses fonctions ([wàs isch](https://fr.wikipedia.org/wiki/Fonction#Mathématiques_et_informatique)?) comme `count()`, `replace()`, `upper-case()` et beaucoup d’autres (sachant qu’on peut aussi en créer soi-même).

Réessayer le précédent exercice en utilisant la fonction `string-join()` pour le résultat du nom d’auteur :

`string-join($auteur, ' et ')`

On peut en tester d’autres :

```
let $document := doc("DossierDeFormation/bookstore.xml")
let $nombreLivres := count($document//*:book)
return($nombreLivres)
```

Ou encore :

```
let $document := doc("DossierDeFormationbookstore.xml)
for $livre in $document//*:book
return(upper-case($livre//*:title) || ", " || replace($livre//*:title, "e", "€"))
```

15. Générer du HTML avec XQuery
```
<div>
<h1>Ma liste de livres</h1>
<ul>
{
    let $document := doc("DossierDeFormation/bookstore.xml")
    for $livre in $document//*:book
    let $auteur := $livre/*:author
    let $titre := $livre/*:title
    let $annee := $livre/*:year

    return
    (
        <li>{string-join($auteur, ' ')}, <em>{data($titre)}</em></li>
    )
}
</ul>
</div>
```

15. Générer du CSV avec XQuery

```
"auteur;livre;année",
(
    let $document := doc("DossierDeFormation/bookstore.xml")
    for $livre in $document//*:book
    let $auteur := $livre/*:author
    let $titre := $livre/*:title
    let $annee := $livre/*:year
    return
    (string-join($auteur, ' ') || ';' || $titre || ';' || $annee)
)
```